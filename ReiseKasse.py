#!/usr/bin/env python3
"""

    to run this program in Debian/jessie do:
    aptitude install python-bottle gunicorn

    if you don't want to use gunicorn (python2 only) read:
    http://bottlepy.org/docs/dev/deployment.html#switching-the-server-backend
"""
from bottle import app, route, run, request, template, debug, static_file, response
from beaker.middleware import SessionMiddleware
import os
import finanzausgleich


## service settings
servehosts = '127.0.0.1'
server = 'auto' # use uwsgi or gunicorn in production environments
serverport = 8008
serverworkers = 1
logo = "logo.png"
basedir = os.getcwd()
debugswitch = False

## initial tool settings
## default values for non-cookie minimal mode
participants_mini = 3
inputfields_mini = 4
fuzzydiff_mini = 5


## config for beaker middleware
session_opts = {
    'session.type': 'file', ## use 'file' or 'memory'
    'session.cookie_expires': 2592000, ## last for 30 days
    'session.data_dir': './sessiondata',
    #'session.secure':True, ## if https is available
    #'session.cookie_domain':'.systemausfall.org', ## for subdomains
    'session.auto': True
}

## css, logo and static files or dev only ...
## better serve these files through httpd proxy like nginx via:
'''
    location /style/ {
        alias /..path_to_this_code../style/ ;
        expires 10d;
    }
'''
@route('/style/default.css')
def server_static():
    return static_file('default.css', root=basedir + '/style')
@route(os.path.join('/style/', logo))
def server_static():
    return static_file(logo, root=basedir + '/style')

## main pages...
@route('/')
def config():
    ## set testcookie for next page
    cookie = request.environ.get('beaker.session')
    cookie['check'] = 1
    cookie.save()

    output = {
        'title': 'neue Tabelle', 'subtitle': 'f&uuml;r den Finanzausgleich erstellen',
        'debugswitch': debugswitch, 'cookiedata': cookie
        }
    return template('config', output=output)



@route('/request', method='POST')
def get_values():
    ## read posted input; use default values if none were given
    vorgang = str(request.forms.get('vorgang'))
    if vorgang == '':
        vorgang = "Zelten an der Ostsee"
    participants = request.forms.get('participants')
    if participants == '' or int(participants) <= 0:
        participants = participants_mini
    inputfields = request.forms.get('inputfields')
    if inputfields == '' or int(inputfields) <= 0:
        inputfields = inputfields_mini
    fuzzydiff = request.forms.get('fuzzydiff')
    if fuzzydiff == '' or int(fuzzydiff) <=0 :
        fuzzydiff = fuzzydiff_mini

    ## see if cookie was set and use it
    cookie = request.environ.get('beaker.session')
    check = get_cookie_value(cookie, 'check')
    if check == 1:
        ## save values to session cookie
        cookie['vorgang'] = vorgang
        cookie['participants'] = int(participants)
        cookie['inputfields'] = inputfields
        cookie['fuzzydiff'] = fuzzydiff
        cookie.save()

    else:
        ## switch to minimal mode with default values
        cookie = "MINIMODE"
        participants = participants_mini
        inputfields = inputfields_mini
        fuzzydiff = fuzzydiff_mini

    output = {
        'title': 'Tabelle', 'subtitle': vorgang,
        'inputfields': int(inputfields), 'participants': int(participants),
        'debugswitch': debugswitch, 'cookiedata': cookie
        }
    return template('request', output=output)


@route('/result', method='POST')
def calc_values():
    names = []
    werte = []
    solifaktoren = []
    result = ""

    cookie = request.environ.get('beaker.session')
    check = get_cookie_value(cookie, 'check')
    if check == 1:
        ## read given values via cookie
        vorgang = get_cookie_value(cookie, 'vorgang')
        participants = int(get_cookie_value(cookie, 'participants'))
        inputfields = int(get_cookie_value(cookie, 'inputfields'))
        fuzzydiff = int(get_cookie_value(cookie, 'fuzzydiff'))

    else:
        ## without cookies switch to minimal mode with default values
        cookie = "MINIMODE"
        vorgang = "MINIMODE"
        participants = participants_mini
        inputfields = inputfields_mini
        fuzzydiff = fuzzydiff_mini

    ## put input values in lists
    for n in range(participants):
        ## get entered names or set a number if none given
        name = request.forms.get('name_%i' % n)
        if name == '':
                name = "Nr. %i" % n
        names.append(name)
        werte.append([])

        ## get financial values
        for w in range(inputfields):
            wert = request.forms.get('name_%i-value_%i' % (n, w))
            if wert == '':
                wert = 0
            werte[n].append(float(wert))
        ## get solidarity factors
        soli = request.forms.get('name_%i-soli' % n)
        if soli == '':
            soli = 1
        solifaktoren.append(float(soli))

    result += "\nNamen: %s" % str(names)
    result += "\nWerte: %s" % str(werte)
    result += "\nSoli: %s" % str(solifaktoren)

    ## let the algorithm work
    fa = finanzausgleich.FinanzAusgleich(vorgang)
    #print(fuzzydiff, werte, solifaktoren)
    #result += fa.calc(fuzzydiff, werte, solifaktoren)
    fa.calc(fuzzydiff, werte, solifaktoren)
    transactions = fa.get_transactions()
    result += str(transactions)
    transtrans = []
    if len(transactions) == 0:
        result += "Mit den eingegebenen Werten ist kein Ausgleich notwendig."
    else:
        ## finally replace numbers with names
        for i in range(len(transactions)):
            transtrans.append([names[int(transactions[i][0])],
                names[int(transactions[i][1])], transactions[i][2] * - 1]
                )

    individual = fa.get_individual()
    if check == 1:
        cookie['result'] = str(individual)
        cookie.save()

    output = {'title': 'anstehende &Uuml;berweisungen f&uuml;r:', 'subtitle': vorgang,
        'debugswitch': debugswitch, 'vorgang': vorgang, 'fuzzydiff': fuzzydiff,
        'transactions': transtrans, 'individual': individual, 'result': result,
        'cookiedata': cookie
        }

    return template('result', output=output)


@route('/delete_session', method='POST')
def delete_session():
    cookie = request.environ.get('beaker.session')
    cookie.delete()
    output = {
        'title': 'Deine Sitzungsdaten', 'subtitle': 'wurden geloescht.',
        'debugswitch': debugswitch, 'cookiedata': cookie
        }
    return template('delete', output=output)

## check for values responding to cookies
def get_cookie_value(cookie, key):
    if key in cookie:
        value = cookie[key]
    else:
        value = '0'
    return value


debug(debugswitch)
## use Middleware for session cookies
ReiseKasse = SessionMiddleware(app(), session_opts)

## lets fetz...
if __name__ == '__main__':
    run(app=ReiseKasse,
        server=server,
        workers=serverworkers,
        host=servehosts,
        port=serverport,
        reloader=True)
else:
    app = application = ReiseKasse
