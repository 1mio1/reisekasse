#!/usr/bin/env python


class FinanzAusgleich:
    def __init__(self, vorgang):
        ## use these standards if no values were given
        self.table = [[135.45, -81.57, 121], [0], [3.34], [61], [67, 37, 95],
            [0], [-123.23, 47.11, 5], [89.11, -11]]
        self.factor = [1, 2, 1, 0.5, 1, 1, 1, 1]
        self.maxdiff = 5
        self.tableresults = [['from', 'to', 'amount']]
        self.output = "\nVorgang: %s\n" % vorgang
        self.transactions = []
        self.individuals = []

    def get_transactions(self):
        return self.transactions

    def get_individual(self):
        return self.individuals

    def calc(self, maxdiff, table, factor):
        #table= self.table
        #factor= self.factor
        #maxdiff= self.maxdiff
        individuals = self.individuals

        table2 = list(range(len(table)))
        table3 = list(range(len(table)))
        table4 = list(range(len(table)))
        x = len(table)
        self.output += "TeilNehmende: %i\n" % x

        for i in range(x):
            table2[i] = round(sum(table[i]), 2)
            individuals.append([i])
        self.output += "Gesamtausgaben: %s\n" % table2
        self.output += "Faktor je TN: %s\n" % factor

        average_head = round(sum(table2) / x, 2)
        self.output += "Ausgaben im Mittel pro TN ohne Faktor: %s Euro\n" \
            % average_head
        average_factor = round(sum(table2) / sum(factor), 2)
        self.output += "Ausgaben im Mittel fuer TN mit Faktor 1: %s Euro\n" \
            % average_factor
        self.output += "Ausgleichstoleranz: %s Euro\n" % maxdiff

        for i in range(x):
            table3[i] = round(table2[i] - average_factor * factor[i], 2)
            table4[i] = round(table2[i] - average_head, 2)
            individuals[i].append(table2[i])
            individuals[i].append(table2[i] * factor[i])
            individuals[i].append(table3[i])
            individuals[i].append(table4[i])
        self.output += "Differenzen Start (ohne Faktor): %s\n" % table4
        self.output += "Differenzen Start (mit Faktor) : %s\n" % table3
        self.individuals = individuals

        for i in range(x * 2):
            maximum = max(table3)
            minimum = min(table3)
            imax = table3.index(maximum)
            imin = table3.index(minimum)
            self.output += "Runde: %s\n" % i
            self.output += "Differenzen: %s\n" % table3

            if maximum <= maxdiff and minimum * -1 <= maxdiff:
                self.output += "Differenzen Ende: %s\n" % table3
                self.output += "Es wurde alles ausgeglichen bei +/- %s Euro. :\
                    )\n" % maxdiff
                return self.output

            elif maximum + minimum > 0 and minimum * -1 >= maxdiff:
                self.output += "Ueberweisung: TN %s an TN %s : %s Euro\n" \
                    % (imin, imax, minimum * -1)
                self.transactions.append([imin, imax, round(minimum,4)])
                table3[imax] = table3[imax] + minimum
                table3[imin] = table3[imin] - minimum

            elif maximum + minimum <= 0 and maximum >= maxdiff:
                self.output += "Ueberweisung: TN %s an TN %s : %s Euro\n" \
                    % (imin, imax, maximum)
                self.transactions.append([imin, imax, round(maximum * -1,2)])
                table3[imax] = table3[imax] - maximum
                table3[imin] = table3[imin] + maximum

            elif maximum >= maxdiff or minimum * -1 >= maxdiff:
                self.output += "Differenzen Ende: %s\n" % table3
                self.output += "Es konnte nicht angemessen ausgeglichen \
                    werden.\n"
                self.transactions.append([6, 6, 6])
                return self.output

        #return self.output
        return self.output
