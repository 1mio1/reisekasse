** easy tool for calculating pooled expenses **

(entspannter solidarischer & dezentraler Finanzausgleich)

# Debian installation #
## Preparations ##

To prepare a server on Debian (wheezy|jessie) to host the ReiseKasse service do the following:

* aptitude install python3 git
* git clone https://bitbucket.org/1mio1/reisekasse.git
* cd reisekasse
* ./createVirtualEnv.sh

Finally edit "service settings" secions in ReiseKasse.py.

## Run the server ##
* source reiseEnv/bin/activate
* ./ReiseKasse.py