#!/bin/sh
rm -r reiseEnv
virtualenv --distribute -p python3 reiseEnv
reiseEnv/bin/pip install -r requirements.txt
echo "run 'source reiseEnv/bin/activate' to use this environment"
