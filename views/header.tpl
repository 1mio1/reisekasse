<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
<meta name=viewport content="width=device-width, initial-scale=1">
<title>ReiseKasse</title>
</head>
<body>
<div id="main">
    <div id="content">

    <div id="rcol">
        <div id="logo">
            <a href="http://senselab.org" title="Sense.Lab e.V."><img src="/style/logo.png" title="S.L. Logo" /></a>
            <a href="/" title="Reise.Kasse"><h1>Reise<span>.Kasse<sup>0.3</sup></span></h1></a>
            <span class="slogan2">entspannter solidarischer &amp; dezentraler Finanzausgleich</span><br />
        </div>

        <div id="topmenu">
                <!-- <a href="/">start</a>&nbsp;<a href="/clean">clean</a>&nbsp;<a href="/status">status</a> -->
        </div>
