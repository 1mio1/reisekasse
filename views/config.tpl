% include('header.tpl')

        <div id="content">

            <div id="about">
                <div class="output">
                    <h3>Was ist <span>das hier?</span></h3>
                    <p>Du warst mit Freunden unterwegs. Ihr hattet gemeinsam eine
                    sch&ouml;ne Zeit und jede_r hat mal hier mal dort was bezahlt.
                    Nun wollt ihr die Kosten solidarisch aufteilen und das ohne
                    zentrale Kasse. Dabei hilft dir die Reise.Kasse.</p>
                    <br />
                    <p>Luc hat des Essen bezahlt, Judith den Zeltplatz und
                    Vicky den Surfkurs. Luc konnte nur drei Tage dabei sein,
                    Judith hat ein viel h&ouml;heres Einkommen als die anderen und
                    Vicky hat nicht auf dem Zeltplatz sondern am Strand
                    geschlafen.</p>
                    <br />
                    <p>Mit der Reise.Kasse kannst du ausrechnen, wer wem noch
                    wieviel Geld gibt, damit ihr nach dem tollen Ausflug auch
                    finanziell entspannt seid.</p>
                </div>
            </div>


            <div id="inputtable">
                <div class="form">
                    <h3>Erstelle eine neue Tabelle <span>f&uuml;r den Finanzausgleich</span></h3>
                    <form action="/request" method="POST">
                        <table>
                            <tr>
                                <td>Titel</td>
                                <td>Anzahl Teilnehmende</td>
                                <td>Wieviel Eingabefelder</td>
                                <td>Toleranz in &euro;</td>
                            </tr>
                            <tr>
                                <td><input name="vorgang" type="text" autofocus placeholder="Zelten an der Ostsee" title="Bezeichnung für die neue Tabelle"></td>
                                <td><input name="participants" type="number" placeholder="3"></td>
                                <td><input name="inputfields" type="number" placeholder="4"></td>
                                <td><input name="fuzzydiff" type="number" placeholder="5"></td>
                            </tr>
                            <tr>
                                <td><input type="submit" value="Los geht's!"></td>
                            </tr>
                        </table>
                    </form>
                </div>
            </div>

            <div id="help">
                <div class="output">
                    <h3>So <span>geht's...</span></h3>
                        <ul>
                            <li>W&auml;hle einen Titel f&uuml;r deine Berechnung</li>
                            <li>Trag' die Anzahl der Teilnehmenden ein</li>
                            <li>Bestimme wieviele Eingabefelder du f&uuml;r Einnahmen &amp;
                            Ausgaben brauchst</li>
                            <li>Stell' die Genauigkeit ein, mit der die
                            Finanzen ausgeglichen werden. Eine Toleranz von
                            &euro;5
                            bedeutet, dass eine Person im Schnitt 5&euro; mehr
                            bezahlen k&ouml;nnte als eine andere, wenn dies die Anzahl der ausgleichenden Transaktionen verringert.</li>
                        </ul>
                        <br />
                        <p>Du kannst auch einfach auf <i>"Los geht's"</i> klicken und dich mit den Standardwerten ausprobieren.</p>
                </div>
            </div>

% include('footer.tpl')
