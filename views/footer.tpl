        %for foo in output:
            %if foo == 'debugswitch' and output[foo] == True:
            <div id="debug">
                <div class="output">
                    <h3>debug <span>values</span></h3>
                    %for foo in output:
                    <pre class="pre_inner">{{foo}} : {{output[foo]}}</pre>
                    %end
                </div>
            </div>
            %end
        %end

        </div>
    </div>
</div>
<div id="footer">
ein Tool von <a href="http://senselab.org">sense.lab e.V.</a>
// Mail: reisekasse (&auml;t) senselab.org
<br />
Willst du das Programm verbessern oder selbst hosten? Gerne,
<a href="https://bitbucket.org/1mio1/reisekasse">bei bitbucket</a> findest du alles
dafür Notwendige.
</div>
</body>
</html>
<link rel="stylesheet" type="text/css" href="/style/default.css" />
