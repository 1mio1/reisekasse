% include('header.tpl')

        <div id="content">

        %for foo in output:
            %if foo == 'cookiedata' and output[foo] == "MINIMODE":
            <div id="minimode">
                <div class="output">
                    <h3>Achtung <span>eingeschr&auml;nkte Nutzung</span></h3>
                    Du kannst diese Anwendung momentan nur eingeschr&auml;nkt nutzen.
                    Aus technischen Gr&uuml;nden werden dir nur 3 Namen- und Zahlenfelder angezeigt.
                    Aktiviere Cookies in deinem Browser f&uuml;r den vollen Funktionsumfang!
                </div>
            <div>
            %end
        %end

            <div id="inputtable">
                <div class="form">
                    <h3>{{output['title']}} <span>{{output['subtitle']}}</span></h3>
                        <form action="/result" method="POST">
                            <table>
                                <tr>
                                    <td>Name</td>
                                    <td>Solifaktor</td>
                                    %for value in range(output['inputfields']):
                                        <td>Ausgaben in &euro;</td>
                                    %end
                                </tr>
                                %for name in range(output['participants']):
                                <tr>
                                    <td><input name="name_{{name}}" type="text" placeholder="Name {{name}}"
                                        %if name == 0: 
                                            autofocus
                                        %end
                                        ></td>
                                    <td><input class="soli" name="name_{{name}}-soli" type="number" min="0" step="0.01" value="1" ></td>
                                    %for value in range(output['inputfields']):
                                        <td><input
                                        name="name_{{name}}-value_{{value}}" type="number" pattern="[0-9](.)[0-9]{2}" step="0.01"></td>
                                    %end
                                </tr>
                                %end
                            </table>
                            <input type="submit" value="Jetzt berechnen!">
                        </form>
                    </div>
            </div>

            <div id="help">
                <div class="output">
                    <h3>So <span>geht's...</span></h3>
                    <p>F&uuml;r jede_n Teilnehmende_n tr&auml;gst du die entsprechenden Ausgaben ein. Daraus wird eine
                    Gesamtsumme und die Kosten pro Nase berechnet - jede_r  nur eine Nase. ;)</p>
                    <br />
                    <p>Mit dem Solifaktor bestimmst du wieviel Kosten eine
                    Person anteilig &uuml;bernimmt. Ein Faktor von 0.5 bedeutet,
                    dass der/diejenige nur halb so viel an den Kosten beteiligt
                    ist, im Vergleich zu Anderen mit Faktor 1. Wenn er/sie
                    z.B. nur die H&auml;lfte der Zeit dabei war. Faktor 2 bedeutet
                    es werden anteilig die doppelten Kosten &uuml;bernommen.</p>
                    <br />
                    <p>Mit dem Solifaktor l&auml;sst sich u.a. eine nach Einkommen gestaffelte Berechnung erstellen.</p>
                    <br />
                    <p>Wenn du die Berechnung startest, wird ermittelt, wer an wen
                    wieviel Geld &uuml;bertragen muss, damit alle gleich an den
                    Kosten beteiligt sind in Abh&auml;ngigkeit von den bislang
                    get&auml;tigten Ausgaben.</p>
                    <br /><br />
                    <p>In einigen F&auml;llen kommt es auch zu Einnahmen (z.B. durch
                    Vermietung). Auch die werden ber&uuml;cksichtigt. Dazu tr&auml;gst
                    sie als negative Zahlen (z.B. -100) in das Ausgabenfeld ein.</p>
                </div>
            </div>

% include('footer.tpl')
