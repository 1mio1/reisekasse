% include('header.tpl')

        <div id="content">

            %for foo in output:
                %if foo == 'cookiedata' and output[foo] == "MINIMODE":
                <div id="minimode">
                    <div class="output">
                        <h3>Achtung <span>eingeschr&auml;nkte Nutzung</span></h3>
                        Du kannst diese Anwendung momentan nur eingeschr&auml;nkt nutzen.
                        Aus technischen Gr&uuml;nden werden dir nur 3 Namen- und Zahlenfelder angezeigt.
                        Aktiviere Cookies in deinem Browser f&uuml;r den vollen Funktionsumfang!
                    </div>
                <div>
                %end
            %end

            <div class="output">
                <h3>anstehende &Uuml;berweisungen f&uuml;r <span>
                    %if output['subtitle']:
                        {{output['subtitle']}}
                    %end
                </span></h3>
                <br />
                %if len(output['transactions']) == 0:
                    <p>Mit den eingegebenen Zahlen ist alles ausgeglichen. :)</p>
                    <br />
                    <p>Durch die eingestellte Toleranz von {{output['fuzzydiff']}}&euro; ist
                    f&uuml;r folgende Differenzen kein Geldverkehr notwendig.</p>
                    <br />
                    <ul>
                    %for i in range(len(output['individual'])):
                        <li>{{output['individual'][i][0]}} :
                        {{output['individual'][i][3]}}&euro;</li>
                    %end
                    </ul>

                %else:
                    %for i in range(len(output['transactions'])):
                        <div class="transactions">von <strong>{{output['transactions'][i][0]}}</strong>
                        an <strong>{{output['transactions'][i][1]}}</strong>
                        so ziemlich genau ungef&auml;hr: <strong>{{output['transactions'][i][2]}}&euro;</strong></div>
                    %end
                %end

                %for foo in output:
                    %if foo == 'cookiedata' and output[foo] != "MINIMODE":
                    <br />
                    <div class="sessiondata">
                        <form action="/delete_session" method="POST">
                            <input class="delete" type="submit" value="Sitzungsdaten jetzt l&ouml;schen!" autofocus>
                        </form>
                    </div>
                    %end
                %end
            </div>

            %for foo in output:
                %if foo == 'debugswitch' and output[foo] == True:
                    <div id="debug">
                        <div class="output">
                            <h3>debug <span>table</span></h3>
                            <table border="1">
                                <tr>
                                    <td>id: </td>
                                    <td>eigene Ausgaben: </td>
                                    <td>faktorisierte Ausg.: </td>
                                    <td>Differenz zum Mittelwert: </td>
                                    <td>offenen Differenz: </td>
                                </tr>
                                %for i in range(len(output['individual'])):
                                    <tr>
                                        %for j in range(len(output['individual'][i])):
                                            <td>{{output['individual'][i][j]}}</td>
                                        %end
                                    </tr>
                                %end
                            </table>
                        </div>
                    </div>
                %end
            %end

% include('footer.tpl')
